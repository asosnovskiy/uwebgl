Template.newBuild.helpers({

});

Template.newBuild.events({'submit .new-build-form': function(event) {

    event.preventDefault();

    var file = $("input.image-bug")[0].files[0];

    //console.log(file);

    var fileObj = new FS.File(file);

    fileObj.title = event.target.title.value;
    fileObj.width = 800;
    fileObj.height = 600;
    fileObj.owner = Meteor.user();

    Zips.insert(fileObj,function(err){
        Router.go('/builds/' + fileObj._id);
    });
}});