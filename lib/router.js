Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loading',
    //waitOn: function() { return Meteor.subscribe('allBuilds'); }
});

Router.map(function() {
    this.route('home', {path: '/'});

    this.route('buildsList', {
        path: '/builds/',
        waitOn: function() {
            return Meteor.subscribe('allBuilds');
        }
    });

    this.route('buildPage', {
        path: '/builds/:_id',
        waitOn: function() {
            return Meteor.subscribe('build', this.params._id);
        },
        data: function(){
            return Zips.findOne({_id: this.params._id});
        }
    });

    this.route('newBuild', { path: 'new'} );
});

Router.map(function() {
    this.route('play', {
        path: '/play/:_id',
        waitOn: function() {
            return Meteor.subscribe('build', this.params._id);
        },
        data: function(){
            return Zips.findOne({_id: this.params._id});
        }
    });
});

Router.map(function() {
    this.route('files', {
        path: '/files/:_id(.*)',
        where: 'server',
        action: function() {
            var fs = Meteor.npmRequire('fs');

            var id = this.params._id;
            var basedir = Meteor.settings.public.zipFolder;

            console.log('will serve static content @ '+ id);

            var path = basedir + id;

            if(fs.lstatSync(path).isDirectory()){
                path = Meteor.npmRequire('path').join(path,'index.html');
            }

            var file = fs.readFileSync(path);

            var headers = {
                //'Content-type': 'text/html'
                //'Content-Disposition': "attachment; filename=" + path
            };

            this.response.writeHead(200, headers);
            return this.response.end(file);
        }
    });
});

Router.onBeforeAction('loading');