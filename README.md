# What is it #

This is meteor project for hosting Unity Web Gl builds.

# How to install? #

1. Install NodeJS - [https://nodejs.org](https://nodejs.org)
2. Install MongoDB - [https://docs.mongodb.org/manual/installation/](https://docs.mongodb.org/manual/installation/)
3. Install Meteor - [https://www.meteor.com/](https://www.meteor.com/)

# How to run/deploy? #

Please, use this command to start app:

```
#!cli

meteor run --settings settings.json
```

For deploy to meteor.com:

```
#!cli

meteor deploy xxx.meteor.com --settings settings.json
```


# How to upload a WebGL build? #

1. Make the web gl build for release;
2. Zip your Release folder;
3. Upload zip archive to your site.