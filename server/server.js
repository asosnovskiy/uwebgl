Zips.allow({
    'insert': function (userId, fileObj) {
        //console.log("insert currentUser:" + userId);
        //console.log(fileObj);

        return userId && userId === fileObj.owner._id;
    },
    'download': function(userId, fileObj) {
        return true;
    },
    'update': function(userId, fileObj){
        //console.log("update currentUser:" + userId);

        return userId && fileObj.owner._id === userId;
    },
    'remove' : function(userId, fileObj){
        //console.log("currentUser:" + userId);

        var file = Meteor.settings.public.zipFolder+fileObj.copies.zips.key;

        //console.log("remove file:" + file);
        return userId && userId === fileObj.owner._id;
    }
});

if (Meteor.isServer) {
    ServiceConfiguration.configurations.remove({
        service: 'vk'
    });

    ServiceConfiguration.configurations.insert({
        service: 'vk',
        appId:   '5254997',      // Your app id
        secret:  'S9xnyu3tsTyEm502dGCy' // Your app secret
    });
}